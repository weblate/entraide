//import PropTypes from "prop-types"
import React from "react"
import {Navbar, Nav, Dropdown, Row, Col} from "react-bootstrap";
import {withPrefix} from "../../.cache/gatsby-browser-entry";
import {changeLocale, IntlContextConsumer, FormattedMessage} from "gatsby-plugin-intl";

const Header = ({ languageName, siteTitle, description }) => (
    <>
        <Navbar bg="transparent" expand="lg">
            {/* <Navbar.Brand href="#home">{siteTitle}</Navbar.Brand> */}
            {/* <Navbar.Toggle aria-controls="basic-navbar-nav" /> */}
            {/* <Navbar.Collapse id="basic-navbar-nav"> */}
                <Nav className="mr-auto" />
                <span>
                    <Dropdown alignRight>
                        <Dropdown.Toggle size="sm" variant="transparent" id="dropdown-basic">
                            <FormattedMessage id="navbar.language" />
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                        <IntlContextConsumer>
                            {({ languages, language: currentLocale }) =>
                                languages.map(language => (
                                    <Dropdown.Item
                                        key={language}
                                        active={language === currentLocale}
                                        onClick={() => changeLocale(language)}
                                    >
                                        {languageName[language]}
                                    </Dropdown.Item>
                                ))
                            }
                        </IntlContextConsumer>
                        <Dropdown.Divider />
                        <Dropdown.Item href="https://weblate.framasoft.org/projects/entraide-chatons/">
                            <FormattedMessage id="navbar.translate" />
                        </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </span>
            {/* </Navbar.Collapse> */}
        </Navbar>
        <header>
            <Row style={{'color': '#666'}} className="justify-content-md-center">
                <Col xs lg="12" xl="6">
                    <div className="align-items-center" style={{margin: '0 auto', maxWidth: '610px'}}>
                        <img src={withPrefix(`/pictures/logo-chatons-big.png`)} alt=""/>
                        <div>
                            <h1>{siteTitle}</h1>
                            <p>{description}</p>
                        </div>
                    </div>
                </Col>
            </Row>
        </header>
    </>
)

// Header.propTypes = {
//     siteTitle: PropTypes.string,
// }

// Header.defaultProps = {
//     siteTitle: ``,
// }

export default Header
