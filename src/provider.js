import React, { useState } from 'react';
import { categories } from "./data/services";
import {graphql, useStaticQuery} from "gatsby";

export const myContext = React.createContext();

const Provider = props => {
    const { dataJson: { nodes } } = useStaticQuery(
        graphql`
            query {
                dataJson(nodes: {elemMatch: {node: {consent: {eq: "1"}}}})  {
                    nodes {
                        node {
                            chaton
                            chaton_website
                            degradability
                            edit_link
                            open
                            endpoint
                            software
                            type
                            weight
                        }
                    }
                }
            }
        `
    )
    const services = nodes.reduce((acc, { node }) => {
        node.category = node.type.split(" (")[0]
        node.URL_site = node.chaton_website;
        const servicesExistants = acc[node.category] || []
        servicesExistants.push(node)
        acc[node.category] = servicesExistants
        return acc
    }, {})

    const randomInstance = instances => {
        const instances2 = instances.map(instance => [
            instance.chaton,
            parseInt(instance.weight, 10) || 1,
        ])
        const keyFound = weightedRand(instances2)
        return instances.find(instance => instance.chaton === keyFound)
    }

    const weightedRand = data => {
        // First, we loop the main dataset to count up the total weight.
        // We're starting the counter at one because the upper boundary of Math.random() is exclusive.
        let total = 1
        for (let i = 0; i < data.length; i += 1) {
            total += data[i][1]
        }
        // Total in hand, we can now pick a random value akin to our
        // random index from before.
        const threshold = Math.floor(Math.random() * total)

        // Now we just need to loop through the main data one more time
        // until we discover which value would live within this
        // particular threshold. We need to keep a running count of
        // weights as we go, so let's just reuse the "total" variable
        // since it was already declared.
        total = 0
        for (let i = 0; i < data.length; i += 1) {
            // Add the weight to our running total.
            total += data[i][1]

            // If this value falls within the threshold, we're done!
            if (total >= threshold) {
                return data[i][0]
            }
        }
        // Should never happen
        return data[0][0]
    }

    const initialInstanceForService = Object.entries(services).reduce(
        (acc, [serviceName, services]) => {
            if (!(serviceName in categories)) return acc;
            acc[serviceName] = randomInstance(services.filter(service => !((categories[serviceName].exclude || []).includes(service.software))))
            return acc
        },
        {}
    )

    const [instanceForService, setInstanceForService] = useState(
        initialInstanceForService
    )

    const anotherInstance = service => {
        let newInstance = instanceForService[service]
        if (services[service].length === 1) return newInstance;
        while (
            newInstance.chaton === instanceForService[service].chaton
            ) {
            if (service in categories) {
                newInstance = randomInstance(services[service].filter(someService => !((categories[service].exclude || []).includes(someService.software))))
            }
        }
        return newInstance
    }

    const changeInstanceForService = service => {
        setInstanceForService({
            ...instanceForService,
            [service]: anotherInstance(service),
        })
    }

    return (
        <myContext.Provider value={{
            instanceForService,
            changeInstanceForService
        }}>
            {props.children}
        </myContext.Provider>
    )
};

export default ({ element }) => (
    <Provider>
        {element}
    </Provider>
);
